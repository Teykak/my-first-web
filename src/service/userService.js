import { api } from "../utils/api";
export const GET_ALL_USERS = async()=>{
    const response = await api.get("users")
    return response.data
}
export const GET_USER_BY_ID= async(id)=>{
    const response = await api.get(`users/${id}`)
    return response.data
}