import React from 'react'

const NotFoundpage = () => {
  return (
    <div className='mt-3 bg-danger'>
        <h1 className='text-center'>Not Found page</h1>
    </div>
  )
}

export default NotFoundpage