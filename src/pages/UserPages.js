import React, { useEffect, useState } from 'react';
import { GET_ALL_USER } from '../Service/userSvice'; 
import UserCard from '../components/UserCard';
import { Placeholder } from 'react-bootstrap';

const UserPages = () => {
    const [users, setUsers] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        GET_ALL_USER()
            .then(response => {
                console.log("Response:", );
                setUsers(response); 
                setIsLoading(false )
            })
            .catch(error => {
                console.log("Error: ", error);
                setIsLoading(false)
            });
    }, []);

    const renderCard = (numberofCard) => {
        let allCard = [];
        for (var i = 0; i < numberofCard; i++) {
            allCard.push(
                <div className='col-4 d-flex justify-content-center'>
                    <Placeholder/>
                </div>
            );
        }
        return allCard;
    };

    return (
        <div className='mt-2 '>
            <h1 className="text-center">User List</h1>
            <div className="row ">
                {isLoading ? 
                (
                    <>
                        {renderCard(6)}
                    </>
                ) : 
                (
                    <>
                        {users.map((user) => (
                            <div className="col-4 d-flex justify-content-center">
                                <UserCard userInfo={user} />
                            </div>
                        ))}
                    </>
                )}
            </div>
        </div>
    );
}

export default UserPages;