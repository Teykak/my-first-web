import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { GET_USER_BY_ID } from '../service/userService'

const ViewUserProfile = () => {
    const{id}=useParams()
    const[user,setUser]=useState({})

    useEffect(()=>{
        GET_USER_BY_ID(id)
        .then(response=>setUser(response))
        .catch(error=>console.log(error))
    },[])
  return (
     <div className='container mt-5 '>

        <div className="d-flex">
            <img className='' width={"300px"} src={user.avatar} alt="this is the user profile " />
            <div className="mt-5">
                <h1> User name is : {user.name} </h1>
                <p className="text-warning"> User role is : {user.role} </p>
                <p>Email is : {user.email}</p>

                <button className='btn btn-warning'>Update </button>
            </div>
        </div>
        
     </div>

  )
}

export default ViewUserProfile