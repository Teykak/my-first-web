import './App.css';
import { BrowserRouter as BigRouter,Routes , Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import "bootstrap/dist/css/bootstrap.min.css";
import UserPages from './pages/UserPages';
import ProductPage from './pages/ProductPage';
import NotFoundpage from './pages/NotFoundpage';
import { useEffect } from 'react';
import MyNarBar from './componentes/MyNarBar';
import ViewUserProfile from './pages/ViewUserProfile';

function App() {
  useEffect(()=>{},[])
  return (
    <BigRouter>
      <MyNarBar/>
      <Routes>
        <Route path='/' index element={<HomePage/>}/>
        <Route path='/user' element={<UserPages/>}/>
        <Route path='/product' element={<ProductPage/>}/>
        <Route path='/user/:id' element={<ViewUserProfile/>}/>
        <Route path='*' element={<NotFoundpage/>}/>
      </Routes>
    </BigRouter>
  );
}

export default App;
